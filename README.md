## Discussion board (Read Only)
The following tasks are implemented in this project.
1. Load posts from API

    * Create two components: postList, postDetail.
    * In postList add dummy data.
    * Use *ngFor on postList.component.html to itrate over list.
    * Pass each data item to a PostDetail component.
    * In postDetail display post title, post body.
2. Load real API data

    * Load real data from server and replace dummy data.
    * Use HttpClient Service
    https://jsonplaceholder.typicode.com/posts
3. Display user data

    * Load user data via the users endpoint: https://jsonplaceholder.typicode.com/users
    * For every post display the curresponding author name.
    * Use Gravatar to display author avatars.
Sample Output:

![alt text](discussionBoard.png)
