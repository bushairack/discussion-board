export interface Post {
  title: string;
  body: string;
  userName: string;
  userId: number;
  email: string;
  user: any;
}
