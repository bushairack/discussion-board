import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post';
import { Md5 } from 'md5-typescript';
import { EmailAddressPipe } from '../email-address.pipe';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css'],
})
export class PostListComponent implements OnInit {
  public topics: any = [];
  constructor(private http: HttpClient) {
    console.log(Md5.init('test'));
    this.http
      .get('https://jsonplaceholder.typicode.com/posts')
      .pipe(
        catchError((err) => {
          console.log('Api call failed', err);
          return of([]);
        })
      )
      .subscribe((posts: Array<Post>) => {
        this.http
          .get('https://jsonplaceholder.typicode.com/users')
          .pipe(
            catchError((err) => {
              console.log('Api call failed', err);
              return of([]);
            })
          )
          .subscribe((users: any[]) => {
            this.mergePostData(posts, users);
            console.log(posts, users);
          });
      });
  }
  private mergePostData(posts: Array<Post>, users: any[]): void {
    for (const post of posts) {
      const userDetails = users.find((user) => {
        return user.id === post.userId;
      });
      post.user = userDetails;
      console.log(post.user.email);
      let emailAddress: string = EmailAddressPipe.prototype.transform(
        post.user.email
      );
      emailAddress = emailAddress.toLowerCase();
      // const emailAddress: string = post.user.email;
      console.log(emailAddress);
      console.log(Md5.init(emailAddress));
    }
    this.topics = posts;
  }

  ngOnInit() {}
}
