import { Component, Input } from "@angular/core";

@Component({
  selector: "hello",
  template: `
    <h1>Weather in {{ name }}</h1>
  `,
  styles: [
    `
      h1 {
        color:#072727;
        margin: 1rem;
        background-color: #3c9e9e;
        padding-right: 1rem;
        padding-left: 1rem;
        border: 1px solid #df0a0a;
        position: relative;
        border-radius: 4px;
        margin-bottom: 1rem;
        text-align: center;
        text-shadow: 2px 2px #52cece;
        box-shadow: 4px 10px #a89292;
        font-style: italic;
        font-family: san-serif;
      }
    `
  ]
})
export class HelloComponent {
  @Input() name: string;
}
