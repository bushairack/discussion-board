import { Component, Input, OnInit } from '@angular/core';
import { Post } from '../models/post';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-post-detail',
  templateUrl: 'post-detail.component.html',
  styleUrls: ['./post-detail.component.css'],
})
export class PostDetailComponent implements OnInit {
  @Input() post: any;
  public details: any;

  ngOnInit(): void {}
}
